from django.contrib import admin

from history.models import History


class HistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


admin.site.register(History, HistoryAdmin)
