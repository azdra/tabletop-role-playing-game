from rest_framework import serializers

from dialogue.models import Dialogue


class DialogueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dialogue
        fields = ['id', 'content', 'type']
