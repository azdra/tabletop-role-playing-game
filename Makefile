start:
	@docker-compose up

stop:
	@docker-compose down

enter:
	@docker-compose exec python_api sh

manage:
	@docker-compose run --rm python_api python manage.py $(filter-out $@,$(MAKECMDGOALS))

migrate:
	make manage migrate

makemigrations:
	make manage makemigrations

fixtures:
	@docker-compose run --rm python_api python manage.py loaddata fixtures/history.yaml
	@docker-compose run --rm python_api python manage.py loaddata fixtures/dialogue.yaml